import { defineStore } from "pinia";
import { obfuscate, Response } from "@/api";

export const useStore = defineStore({
  id: "obfuscate",
  state: () => ({
    loading: false,
    text: "",
    threshold: 500,
    obfuscatedText: "",
  }),
  actions: {
    async updateObfuscatedText(): Promise<void> {
      if (!this.text) {
        this.obfuscatedText = "";
        return;
      }
      this.loading = true;
      obfuscate({
        text: this.text,
        threshold: this.threshold,
      })
        .then((response: Response) => (this.obfuscatedText = response.text))
        .finally(() => setTimeout(() => (this.loading = false), 300));
    },
  },
});
