// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { obfuscate as obfuscateImpl } from "api-client";

export type Request = {
  text: string;
  threshold: number;
};

type Token = {
  word: string;
  punctuation: string;
  number: boolean;
  name: boolean;
};

export type Response = {
  text: string;
  tokens: Token[] | undefined;
};

export const obfuscate = (request: Request): Promise<Response> =>
  obfuscateImpl(request);
