import { Request, Response } from "../index";
import axios, { AxiosResponse } from "axios";

export const obfuscate = (request: Request): Promise<Response> => {
  return axios
    .post("/api", request)
    .then((response: AxiosResponse<Response>) => response.data);
};
