import { Request, Response } from "../index";

const timeoutMillis = 500;

export const obfuscate = (request: Request): Promise<Response> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        text: request.text.split("").reverse().join(""),
      } as Response);
    }, timeoutMillis);
  });
};
