// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");

module.exports = {
  chainWebpack: (config) => {
    config.resolve.alias.set(
      "api-client",
      path.resolve(__dirname, `src/api/${process.env.VUE_APP_API_CLIENT}`)
    );
  },
};
