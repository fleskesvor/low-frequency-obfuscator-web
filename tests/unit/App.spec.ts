import { mount } from "@vue/test-utils";
import App from "@/App.vue";
import { Request } from "@/api";
import { obfuscate as mockObfuscate } from "@/api/mock";

jest.mock("@/api", () => ({
  obfuscate: (request: Request) => mockObfuscate(request),
}));
jest.mock("@/store", () => ({
  useStore: () => {
    return {
      threshold: 600,
    };
  },
}));

describe("App.vue", () => {
  it("renders elements when passed", () => {
    const wrapper = mount(App);

    const inputArea = wrapper.find("textarea:first-of-type");
    expect(inputArea).toBeDefined();
    expect(inputArea.attributes("placeholder")).toEqual("Skriv her");

    const slider = wrapper.find("#slider");
    expect(slider).toBeDefined();
    expect(slider.text()).toEqual("600");
  });
});
